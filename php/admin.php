<?php
require_once("Database.php");
$resultado = Database::getAllUsuarios();
$resultado1 = Database::getAll();
$resultado2 = Database::getAllRol();
$resultado3 = Database::getAllUsuariosProductos ();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon"
        href="../Imagenes/emblema.png">
    <title>ADMIN</title>
    <link rel="stylesheet" href="../css/admin.css">
</head>
<body>
    <p class="encabezado">USUARIOS</p>
    <table class="container">
        <thead>

            <tr>
                <th><h1>ID</h1></th>
                <th><h1>Nombre</h1></th>
                <th><h1>Apellido</h1></th>
                <th><h1>Numero</h1></th>
                <th><h1>Direccion</h1></th>
                <th><h1>Edad</h1></th>
                <th><h1>Correo electrónico</h1></th>
                <th><h1>id_Rol</h1></th>
            </tr>
        </thead>
        <tbody>
        <?php 
  foreach ($resultado as $fila) {
    echo "<tr>";
    echo "<td>" . $fila["id"] . "</td>";
    echo "<td>" . $fila["nombre"] . "</td>";
    echo "<td>" . $fila["apellido"] . "</td>";
    echo "<td>" . $fila["numero"] . "</td>";
    echo "<td>" . $fila["direccion"] . "</td>";
    echo "<td>" . $fila["edad"] . "</td>";
    echo "<td>" . $fila["correo_electronico"] . "</td>";
    echo "<td>" . $fila["id_rol"] . "</td>";
    echo "</tr>";
  }
  
     ?>
        </tbody>
    </table>
    <p class="encabezado">PRODUCTOS</p>
    <table class="container">
        <thead>
            <tr>
                <th><h1>ID</h1></th>
                <th><h1>Nombre</h1></th>
                <th><h1>Precio</h1></th>
                <th><h1>Stock</h1></th>
        </thead>
        <tbody>
        <?php 
  foreach ($resultado1 as $fila) {
    echo "<tr>";
    echo "<td>" . $fila["id"] . "</td>";
    echo "<td>" . $fila["nombre"] . "</td>";
    echo "<td>" . $fila["precio"] . "</td>";
    echo "<td>" . $fila["stock"] . "</td>";
    echo "</tr>";
  }
  
     ?>
        </tbody>
    </table>
    <p class="encabezado">USUARIOS_HAS_PRODUCTOS</p>
    <table class="container">
        <thead>
            <tr>
                <th><h1>ID</h1></th>
                <th><h1>IDProducto</h1></th>
                <th><h1>IDUsuario</h1></th>
                <th><h1>Cantidad</h1></th>
                <th><h1>Fecha de pedido</h1></th>
            </tr>
        </thead>
        <tbody>
        <?php 
  foreach ($resultado3 as $fila) {
    echo "<tr>";
    echo "<td>" . $fila["id"] . "</td>";
    echo "<td>" . $fila["id_producto"] . "</td>";
    echo "<td>" . $fila["id_usuario"] . "</td>";
    echo "<td>" . $fila["cantidad"] . "</td>";
    echo "<td>" . $fila["fecha_pedido"] . "</td>";
    echo "</tr>";
  }
  
     ?>
        </tbody>
    </table>
    <p class="encabezado">ROL</p>
    <table class="container">
        <thead>
            <tr>
                <th><h1>ID</h1></th>
                <th><h1>Nombre Rol</h1></th>
            </tr>
        </thead>
        <tbody>
        <?php 
  foreach ($resultado2 as $fila) {
    echo "<tr>";
    echo "<td>" . $fila["id"] . "</td>";
    echo "<td>" . $fila["nombre"] . "</td>";
    echo "</tr>";
  }
     ?>
        </tbody>
    </table>
</body>
</html>