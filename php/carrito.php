<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="author" content="Jose Vera, Juan Robayo y David Chavarría">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../fontawesome-free-6.2.1-web/css/all.css">
    <link rel="stylesheet" href="../css/carrito.css">
    <link rel="icon" type="image/x-icon" href="Imagenes/emblema.png">
    <title>TATES FC</title>
</head>

<body>
    <header>
        <p>TU COMPRA TATE</p>
    </header>
    <main>
        <div class="titulos">
            <div class="elemento-hijo-titulo">Descripción</div>
            <div class="elemento-hijo-titulo">Talla</div>
            <div class="elemento-hijo-titulo">Calidad</div>
            <div class="elemento-hijo-titulo">Remover</div>
            <div class="elemento-hijo-titulo">Precio</div>
        </div>

        <div class="linea"></div>

        <div class="producto1">
            <div class="elemento-hijo-producto1">
                <div class="foto1">
                    <img class="pantalon" src="carpeta img/camisetalocal.png" alt="">
                </div>
                <div class="descripcion">
                    <p class="titulo1">Camiseta Local TATES 22/23</p>
                    <p class="code1">Codigo Producto: 1991X</p>
                </div>
            </div>
            <div class="elemento-hijo-producto1">
                <div class="cuadrado">
                    <p class="dinero-talla-cantidad">M</p>
                </div>
            </div>
            <div class="elemento-hijo-producto1">
                <p class="dinero-talla-cantidad1">+</p>
            </div>
            <div class="cuadrado2">
                <p class="dinero-talla-cantidad">2</p>
            </div>
            <div class="cuadrado3">
                <p class="dinero-talla-cantidad">-</p>
            </div>
        </div>
        <div class="elemento-hijo-producto1">
            <div class="cuadrado">
                <p class="dinero-talla-cantidad">x</p>
            </div>
        </div>
        <div class="elemento-hijo-producto1">
            <div class="cuadrado4">
                <p class="dinero-talla-cantidad">84,99€</p>
            </div>
        </div>
        </div>

        <div class="linea"></div>

        <div class="producto2">
            <div class="elemento-hijo-producto2">
                <div class="foto2">
                    <img class="top" src="carpeta img/guantesjose.png" alt="">
                </div>
                <div class="descripcion">
                    <p class="titulo2">Guantes de Jose Vera firmados</p>
                    <p class="code2">Codigo Producto: 2112Y</p>
                </div>
            </div>
            <div class="elemento-hijo-producto2">
                <div class="cuadrado">
                    <p class="dinero-talla-cantidad">16</p>
                </div>
            </div>
            <div class="elemento-hijo-producto2">
                <div class="cuadrado1">
                    <p class="dinero-talla-cantidad1">+</p>
                </div>
                <div class="cuadrado2">
                    <p class="dinero-talla-cantidad">1</p>
                </div>
                <div class="cuadrado3">
                    <p class="dinero-talla-cantidad">-</p>
                </div>
            </div>
            <div class="elemento-hijo-producto2">
                <div class="cuadrado">
                    <p class="dinero-talla-cantidad">x</p>
                </div>
            </div>
            <div class="elemento-hijo-producto2">
                <div class="cuadrado4">
                    <p class="dinero-talla-cantidad">1.300€</p>
                </div>
            </div>
        </div>

        <div class="linea"></div>

        <div class="precios">
            <div class="caja">
                <div class="costo">
                    <p class="descripcion">Descuento</p>
                </div>
                <div class="precio">
                    <p class="dinero-talla-cantidad">0,00€</p>
                </div>
            </div>
            <div class="caja">
                <div class="costo">
                    <p class="descripcion">Domicilio</p>
                </div>
                <div class="precio">
                    <p class="dinero-talla-cantidad">1,99€</p>
                </div>
            </div>
            <div class="caja">
                <div class="costo">
                    <p class="descripcion">Subtotal</p>
                </div>
                <div class="precio">
                    <p class="dinero-talla-cantidad">1.384,99€</p>
                </div>
            </div>
            <div class="caja1">
                <div class="costo">
                    <p class="descripcion">Total</p>
                </div>
                <div class="precio">
                    <p class="dinero-talla-cantidad">1.386,98€</p>
                </div>
            </div>
        </div>

        <div class="mensaje">Si tiene un codigo de descuento, introduce aqui:</div>

        <div class="descuentos">
            <div class="rectangulo1">
                <p class="descripcion1">Ingresa el codigo de descuento</p>
            </div>
            <div class="rectangulo2">
                <p class="descripcion2">Aplicar descuento</p>
            </div>
            <div class="rectangulo3">
                <div class="cesta">
                    <div class="checkout">
                        <p class="descripcion3">Pagar</p>
                    </div>
                    <div class="icono">
                        <i class="fas fa-shopping-basket"></i>
                    </div>
                </div>
                <div class="update-quantity">
                    <p class="descripcion4">Actualizar</p>
                </div>
                <div class="continue-shopping">
                    <p class="descripcion4">Continuar comprando</p>
                </div>
            </div>
        </div>
    </main>
</body>

</html>