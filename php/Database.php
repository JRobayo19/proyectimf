<?php 
        class Database{
             public static function conectar(){
                $driver = "mysql";
                $host = "127.0.0.1";
                $port = "3306";
                $user = "root";
                $password = "";
                $db= "pfc";

                $dns = "$driver:dbname=$db;host=$host;port=$port";
        
                try {
                    $gbd = new PDO ($dns, $user, $password);
                } catch (PDOException $e) {
                     echo "Error: fallo en la conexión" . $e->getMessage();
                 }
                return $gbd;
        }

        public static function getAll(){
            /*
                1. Conecta
                2. Realiza la query
            */ 
            $sql = "SELECT * FROM Productos";
         
            $resultado = self::conectar()->query($sql);

            return $resultado;
        }


        public static function getAllUsuarios() {
            /*
                1. Conecta
                2. Realiza la query
            */ 
            $sql = "SELECT * FROM usuarios";
         
            $resultado1 = self::conectar()->query($sql);
    
            return $resultado1;
        }
    

        public static function getAllRol(){
            /*
                1. Conecta
                2. Realiza la query
            */ 
            $sql = "SELECT * FROM Rol";
         
            $resultado = self::conectar()->query($sql);

            return $resultado;
        }

        public static function getAllUsuariosProductos(){
            /*
                1. Conecta
                2. Realiza la query
            */ 
            $sql = "SELECT * FROM usuario_has_productos";
         
            $resultado = self::conectar()->query($sql);

            return $resultado;
        }

    }
       
?>